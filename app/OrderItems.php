<?php

namespace staxoweb;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{


    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['usersId', 'ordersId', 'productsId', 'notes'];

    /**
     * The rules for creating new statuses.
     *
     * @var string
     */
    public static $rules = [
            'usersId'       => 'required|numeric|exists:UsersId',
            'ordersId'      => 'required|numeric|exists:OrdersId',
            'productsId'    => 'required|numeric|exists:ProductsId',
            'notes'         => 'min:4'
        ];

    /**
     * Get the operation that has this note.
     */
    public function orders()
    {
        return $this->belongsTo('App\Orders');
    }

    /**
     * Get the user that made this note.
     */
    public function users()
    {
        return $this->belongsTo('App\Users');
    }

    /**
     * Get the operation that has this note.
     */
    public function products()
    {
        return $this->belongsTo('App\Products');
    }

}