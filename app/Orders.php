<?php

namespace staxoweb;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable fields
     * 
     * @var array
     */
    protected $fillable =  ['status', 
                        'usersId', 'addressesId', 'customersId', 
                        'req_date', 'est_date', 
                        'description', 'receipt'];

    /**
     * The rules for creating new operations.
     *
     * @var string
     */
    public static $rules = [
        'status'       => 'exists:statusesId',
        'description'  => 'required|min:5',
        #'req_date'    => 'required|date|after:tomorrow', 
        #'est_date'    => 'date', 
        'usersId'      => 'required|numeric|exists:usersId',
        'customersId'  => 'required|numeric|exists:customersId',
        'addressesId'  => 'required|numeric', # might get deleted after submision so not worth risking error
        'receipt'      => 'required|min:3'
    ];

    /**
    * Get the users associated with the operation.
    */
    public function users()
    {
        return $this->belongsTo('App\Users');
    }

    /**
    * Get the addresses associated with the operation.
    */
    public function addresses()
    {
        return $this->belongsTo('App\Addresses');
    }

    /**
    * Get the items for the operation.
    */
    public function products()
    {
        return $this->hasMany('App\Products');
    }

    /**
    * Get the operations records associated with the operation.
    */
    public function notes()
    {
        return $this->hasMany('App\Notes', 'operations_id', 'id');
    }

    /**
    * ...
    */
    public function myOrders()
    {
        return $this->belongsTo('App\Users', 'user_id')->open();
    }

    /**
     * Scope a query to only include active(?) orders
     * require an action.
     *
     * @return Illuminate\Database\EloquentBuilder
     */
    public function scopeOpen($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Scope a query to only include completed(?) orders
     *
     * @return Illuminate\Database\EloquentBuilder
     */
    public function scopeClosed($query)
    {
        return $query->where('status', 0)
                     #->where('confirmed_by', Auth::user()->customers_id)
                     ;
    }

}