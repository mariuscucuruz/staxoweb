<?php

namespace staxoweb;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable fields
     * 
     * @var array
     */
    protected $fillable =  ['usersId', 'price', 
                            'manufacturer', 'description', 'model'
                            #, ''
                            ];

    /**
     * The rules for creating new stock items.
     *
     * @var string
     */
    public static $rules = array(
        #'customersId'  => 'required|numeric|exists:customers',
        'usersId'      => 'required|numeric|exists:users',
        'price'        => 'required|numeric',
        'manufacturer' => 'required|min:5',
        'description'  => 'required|min:5',
        'model'        => 'required|min:5',
    );

    /**
    * Get the operations records associated with the customer.
    */
    public function users()
    {
        return $this->belongsTo('App\Users');
    }

    /**
     * Scope a query to show all models
     *
     * @return Illuminate\Database\EloquentBuilder
     */
    public function scopeAll($query)
    {
        return $query->where('model', '*');
    }
}