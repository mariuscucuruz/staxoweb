<?php

namespace staxoweb;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    #use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'level', 'status'
        #, 'phone', 'mobile', 'etc...'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The rules for creating new users.
     *
     * @var string
     */
    public static $rules = [
        'name'          => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u||required|min:2', 
        'email'         => 'required|email|unique:users',
        'password'      => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|min:8', 
        'level'         => 'required|regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|min:2', 
        'status'        => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|min:1', 
        #'phone'         => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|min:4', 
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}