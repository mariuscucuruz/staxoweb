<?php

namespace staxoweb\Http\Controllers\Auth;

use staxoweb\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    #use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectAfterLogout = '/login';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        #$this->auth = $auth;
        #$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Upon authentication it checks if account is active 
     * otherwise denies logon with error message.
     *
     * @return void
     */
    public function authenticated (Request $request)
    {
        $user = \Auth::user();
        if ($user->status == 0)
        {
            \Auth::logout();
            return redirect($this->redirectAfterLogout)
                                ->withInput($request->only('email', 'remember'))
                                ->withErrors([
                                        #'email'   => $this->getFailedLoginMessage(),
                                        'inactive' => 'Account inactive! Please contact your fleet manager to enable your user account.',
                                    ]);
        }
        else {
            return redirect($this->redirectTo);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            #'username'     => 'required|max:100|unique:users',
            'name'          => 'required|min:2|max:100',
            'email'         => 'required|email|max:100|unique:users',
            'password'      => 'required|min:6|confirmed',
            'customers_id'  => 'required|numeric',
        ]);
    }

    /**
     * Show the application registration form
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $url_parameters = $request->route()->parameters();
        $custid         = $url_parameters['custid'];

        return view('auth.register', compact('custid'));
    }


    /**
     * Show the application registration form
     * and override the getRegister method on 
     * the RegistersUsers trait.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister(Request $request)
    {
        #dd($request->route()->parameters());
        #return view('auth.register')->with($data);
    }

    /**
     * Handle a registration request by overridding 
     * the postRegister method of the 
     * RegistersUsers trait.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        #$request->request->add(['date_of_birth' => implode('-', $request->only('year', 'month', 'day'))]); ##nicely done!!
        # validate user fields
        $validator = $this->validator($request->all());
        if ($validator->fails()) 
        {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }


        /**
         * save new user record:
         */
        $userData = new User($request->all());
        $userData->save();



        return redirect("registration-confirmed/")
                    ->withInput()
                    ->with('message', 'Your request has been updated.');

        #Auth::login($this->create($request->all()));
        #return redirect($this->redirectPath());
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            #'username'     => $data['username'],
            'name'          => $data['name'],
            'email'         => $data['email'],
            'password'      => bcrypt($data['password']),
            'customers_id'  => $data['customerid'], #changed var name for added security
        ]);
    }


}
