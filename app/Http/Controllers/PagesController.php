<?php
namespace staxoweb\Http\Controllers;

use staxoweb\Orders;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    /**
     * Show the static page registration-confirmed.
     *
     * @return \Illuminate\Http\Response
     */
    public function reg_confirmed()
    {
        return view('auth.confirmation');
    }


    /**
     * Show the static page about.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('about');
    }

    /**
     * Show the static page terms-and-conditions.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms()
    {
        return view('terms');
    }

    /**
     * Show another static page:
     *
     * @return \Illuminate\Http\Response
     */
    public function anotherpage()
    {
        return view('another');
    }


}
