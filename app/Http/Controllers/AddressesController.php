<?php

namespace staxoweb\Http\Controllers;


use Illuminate\Http\Request;

use staxoweb\User;
use staxoweb\Customers as Customers;
use staxoweb\Orders;
use staxoweb\Addresses;

use staxoweb\Http\Requests;

#use staxoweb\Http\Controllers\Controller;
#use staxoweb\Http\Requests\LoginRequest;
#use Illuminate\Support\Facades\Redirect;
#use Validator;
#use Session; guard
use Auth;
use Illuminate\Contracts\Auth\Guard;
#use Illuminate\Foundation\Auth\ThrottlesLogins;
#use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class AddressesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->middleware('web');
        #$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses   = Addresses::all();
        dd($addresses);

        /*foreach ($addresses as $address)
        {
            $addressData[$address->id]['address']   = Addresses::find($address->id);

            $addressData[$address->id]['customer']  = Customers::find($address->customers_id);

            $addressData[$address->id]['user']      = User::find($address->created_by);
        }*/
        #dd($addressData);

        $pagetitle = 'Addresses list';

        return view('addresses.index', compact('pagetitle', 'addresses'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) 
    {
        if (!$this->isUserAllowed($id))
            return view('errors.404');

        $address = Addresses::find($id);
        #dd($address);

        $pagetitle = 'Address details';
        return view('addresses.view', compact('pagetitle', 'address'));
    }

   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
   public function create()
   {
        $user       = Auth::user();
        $pagetitle  = 'Create a New Address for '. $user->name;
        return view('addresses.create', compact('pagetitle', 'user'));
   }

   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
   public function store(Request $request)
   {
        $input = $request->all();
        #dd($input);

        $validator = Validator::make($input, Addresses::$rules);
        #dd($validator->errors()->all());

        // Validate and store the entry:
        if ($validator->passes())
        {
            if ($request->_referer)
            {
                $redirect = str_ireplace(url('/'), '', $request->_referer);
                unset($input['_referer']);
                #$request->flashOnly(['_referer']);
            }
            else {
                $redirect = 'dashboard/';
            }

            # create record
            addresses::create($input);

            return redirect($redirect)
                        ->with('message', 'Your new address has been saved.')
                        ->withInput();
        }

        #dd($validator->errors()->all());
        $messages = $validator->errors()->all();

        return redirect()->back()
                        ->withInput()
                        ->withErrors($messages)
                        ->with('message', 'There were validation errors');
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function edit($id)
   {
        if (!$this->isUserAllowed($id))
            return view('errors.404');

        $address = Addresses::findOrFail($id);
        #dd($address);

        if (!$address)
            return redirect ('addresses/');

        $pagetitle = 'Edit Address';

        return view('addresses.edit', compact('pagetitle', 'address'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function update($id, Request $request)
   {
        if (!$this->isUserAllowed($id))
            return view('errors.404');


        $pagetitle = 'Save Address';
        $address = Addresses::findOrFail($id);

        $input = $request->all();

        $messages = [];
        $validator = Validator::make($input, Addresses::$rules);
        #dd($validator);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) 
        {
            #dd($validator->errors()->all());
            $messages = $validator->errors()->all();

            return redirect()->back()
                            ->withInput()
                            ->withErrors($messages)
                            ->with('message', 'There were validation errors');
        }

        $address->fill($input)->save();
        #dd('validated?');

        return redirect("dashboard/")
                    ->with('message', 'The address has been updated.')
                    ->withInput();
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function trash($id, Request $request)
   {
        $address = Addresses::findOrFail($id);
        $address->delete();

        if ( $request->ajax() ) 
            return response(['msg' => 'Address traashed', 'status' => 'success']);

        return redirect("dashboard/")
                    ->with('message', 'Address has been trashed.')
                    ->withInput();
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \staxoweb\Addresses  $addresses
     * @return \Illuminate\Http\Response
     */
   #public function destroy(Addresses $addresses)
   public function destroy($id, Request $request)
   {
        $address = Addresses::findOrFail($id);
        $address->delete();
        #Addresses::find($id)->delete();

        if ( $request->ajax() ) 
            return response(['msg' => 'Address deleted', 'status' => 'success']);

        return redirect("dashboard/")
                    ->with('message', 'Address deleted!')
                    ->withInput();
   }
}
