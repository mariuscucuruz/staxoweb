<?php

namespace staxoweb\Http\Controllers;

use staxoweb\User;
use staxoweb\Customers;
use staxoweb\Addresses;
use staxoweb\Orders;
use staxoweb\Products;

use Validator;
use Session;
use Auth;

use staxoweb\Http\Requests\LoginRequest;
use staxoweb\Http\Controllers\Controller;
use staxoweb\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Hash;
#use DB;

class UserController extends Controller
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    public static $rules = [
        'name'          => 'min:6|required',
        'email'         => 'required|unique:users|email',
        'password'      => 'min:6|required|confirmed',
        'level'         => 'numeric|required', 
        'status'        => 'numeric', 
    ];

    public function __construct() 
    {
        $this->middleware('auth');
    }

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index() 
    {
        $users = User::all();
        #$users = Auth::user();
        #dd($users);

        $pagetitle = 'USERS';
        return view('users.index', compact('users', 'pagetitle'));
    }

   /**
    * Display an entry of the resource.
    *
    * @return Response
    */
    public function view($id = 0) 
    {
        $pagetitle = 'View User Record';

        $user = Auth::user();
        if ($id == 0)
            $id = $user->id;

        dd($user);
        /**
         * if current user is operator or less,
         * and attempting to view someone else,
         * then show their own details
        **/
        if ($user->level < 40 && $id <> $user->id)
            $id = $user->id;

        try {
            ## reset user resource:
            $user = User::find($id);
            #dd($user);

            $customer = User::find($user->customers_id)->customers;
            if ($customer) 
            {
                #dd($customer);

                $accounts = Customers::findOrFail($customer->id)->accounts;
                #dd($accounts);

                #$consumable_equips = Customers::findOrFail($customer->id)->consumable_equips;
                #dd($consumable_equips);

                $operations = $user->myOperations();
                #$operations = Customers::find($customer->id)->operations;
                #dd($operations);
            }

            return view('users.view', compact('user', 'pagetitle', 'customer', 'accounts', 'operations'));
        }
        catch (Exception $e) {
            Session::flash('message', $e);
            return view('errors.404', compact(['e']));
        }
    }

   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
   public function create()
   {
        $pagetitle = 'Add User';
        return view('users.create', compact('pagetitle'));
   }

   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
   public function store(Request $request)
   {
        $pagetitle = 'Save User Data';

        $input = $request->all();
        #dd($input);

        $messages = [];

        $rules = User::$rules;
        $rules['password'] = 'min:6|required|confirmed';
        $validator = Validator::make($input, $rules);
        #dd($validator);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) 
        {
            #dd($validator->errors()->all());
            return redirect()->back()
                        ->withErrors($validator->errors()->all()) // send back all errors to the login form
                        ->withInput();
        }
        #elseif ($validator->passes()) {}
        else {

                $input['password'] = Hash::make($request->password);
                User::create($input);

                $messages['success'] = 'Record successfully added!';
                #Session::flash('flash_message', $messages);
                return redirect('users')->with('message', $messages);
        }

        return redirect()->back();
   }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function show($id = 0)
   {
        $pagetitle      = 'View User Record';
        $user           = [];
        $customer       = [];
        $accounts       = [];
        $operations     = [];

        $user = Auth::user();
        if ($id == 0)
            $id = $user->id;

        if ($user->level < 40 && $id <> $user->id)
            $id = $user->id;

        try {
            $user       = User::findOrFail($id);
            $customer   = $user->customers;

            if ($customer) 
            {
                #dd($customer);

                $accounts = $customer->accounts;
                #dd($accounts);

                $operations = $customer->operations;
                #dd($operations);

                $addresses = $customer->addresses;
                #dd($addresses);
            }

            return view('users.view', compact('user', 'pagetitle', 'customer', 'accounts', 'operations', 'addresses'));
        }
        catch (Exception $e) {
            return view('errors.404', compact(['e']));
        }


   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function edit($id = 0)
   {
        $pagetitle = 'Update User Accounts';
        $messages = @$errors;

        $user = Auth::user();
        if ($id == 0)
            $id = $user->id;

        if ($user->level < 40 && $id <> $user->id)
            $id = $user->id;

        try {
            $user = User::find($id);

            return view('users.edit', compact('user', 'messages'));
        }
        catch (Exception $e) {
            Session::flash('message', $e);
            return view('errors.503', compact('e'));
        }
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function update($id, Request $request)
   {
        # hack protect:
        $current_user = Auth::user();
        if ($current_user->level < 40 && $id <> $current_user->id)
        {
            $message = 'Caught hacking attempt!';
            Session::flash('message', $message);
            return view('errors.404', compact('message'));
        }

        $pagetitle = 'Save User Data';
        $user = User::findOrFail($id);

        $input = $request->all();
        #dd($input);
        
        $messages = [];

        $rules = User::$rules;
        $rules['email'] .= ',email,'. $user->id;


        if (@$request->password != '')
        {
            # if password is set then add validation rule:
            $rules['password'] = 'min:6|required|confirmed';
        }
        else {
            # no pass set so rmove this from validation:
            unset($input['password']);
            unset($input['password_old']);
            unset($input['password_confirmation']);
        }

        $validator = Validator::make($input, $rules);
        if (@$request->password != '')
        {
            # throw error is old password does not match:
            if (bcrypt($request->password_old) != $user->password)
            { 
                $validator->errors()->add('password', 'You did not match the old password!');
            }
            else {
                $input['password'] = bcrypt($input['password']);
            }
        }
        #dd($validator);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) 
        {
            #dd($validator->errors()->all());
            $messages = $validator->errors()->all();

            return redirect('users/'. $user->id.'/edit')
                            ->withErrors($messages)
                            ->withInput();

            /*return view("users/$id/edit", compact('user', 'messages'))#->back()
                ->withErrors($validator) // send back all errors to the login form
                ->with(['messages', $messages])
                ->withInput();*/
        }
        else {
            $user->fill($input)->save();

            if (@$request->password != '')
            {
                $user->fill([
                        'password' => Hash::make($request->password)
                    ])->save();
            }
            #dd('validated?');
            return redirect("dashboard/")
                        ->with('message', 'User details have been saved.');
        }

        if ( $request->ajax() ) 
            return response(['msg' => 'not yet allowed via ajax', 'status' => 'error']);

        return redirect('users/'. $user->id);
   }

   /**
    * trash / soft delete the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function trash($id)
   {
        $user = User::findOrFail($id);
        $user->delete();
        #dd($user);

        if ( $request->ajax() ) 
            return response(['msg' => 'User trashed', 'status' => 'success']);

        return redirect("users/");
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function destroy($id)
   {
        $user = User::findOrFail($id);
        $user->delete();

        if ( $request->ajax() ) 
            return response(['msg' => 'User deleted', 'status' => 'success']);

        return redirect("users/");
   }
}
