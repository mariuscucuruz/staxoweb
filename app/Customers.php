<?php

namespace staxoweb;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{


    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable fields
     * 
     * @var array
     */
    protected $fillable =  ['usersId', 'name', 'bio'];

    /**
     * The rules for creating new operations.
     *
     * @var string
     */
    public static $rules = [
        'usersId'      => 'required|numeric|exists:usersId',
        'name'         => 'min:1',
        'bio'          => 'min:1'
    ];

    /**
    * Get the users associated with the operation.
    */
    public function users()
    {
        return $this->belongsTo('App\Users');
    }

    /**
    * Get the addresses associated with the operation.
    */
    public function addresses()
    {
        return $this->hasMany('App\Addresses');
    }

    /**
    * Get the items for the operation.
    */
    public function products()
    {
        return $this->hasMany('App\Products');
    }
}