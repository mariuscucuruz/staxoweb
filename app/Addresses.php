<?php

namespace staxoweb;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addresses extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'users_id', 'main', 'name', 
        'street1', 'street2', 
        'postcode', 'county', 'country'
        #, ''
    ];

    /**
     * The rules for creating new addresses.
     *
     * @var string
     */
    public static $rules = [
            'customers_id'  => 'required|numeric|exists:customers,id',

            #'name'         => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|required|min:4', 
            'street1'       => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|required|required|min:4', 
            'street2'       => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u', 
            'postcode'      => 'regex:/^[\pL\pM\pN\n\r\s\.\'\"\\:\/,\(\)@&?!_-]+$/u|required|min:4', 
            'county'        => 'required|min:4', 
            'country'       => 'min:2', 
            'main'          => 'numeric', 
            #'',
        ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the customer that owns the address.
     */
    public function customer()
    {
        return $this->belongsTo('App\Customers');
    }

}