@extends('layouts.main')

@section('pagetitle') {{ $pagetitle }} @endsection

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">Add user</div>

                <div class="panel-body">

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

{!! Form::open(['action'=>'UserController@store']) !!}

    <div class="form-group form-horizontal">
        <div class="form-group">
            {!! Form::label('name', 'Full Name', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('name', null, ['class' => 'form-control']) !!}
            </div>  
        </div>

        <div class="form-group">
            {!! Form::label('phone', 'Phone', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('phone', null, ['class' => 'form-control']) !!}
            </div>  
        </div>
        <div class="form-group">
            {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('mobile', null, ['class' => 'form-control']) !!}
            </div>  
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>  
        </div>

        <hr />
        <div class="form-group">
            {!! Form::label('password', 'Password', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password', null, ['class' => 'form-control']) !!}
            </div>  
        </div>
        <div class="form-group">
            {!! Form::label('password_confirmation', 'Confirm password', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password_confirmation', null, ['class' => 'form-control']) !!}
            </div>  
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::submit('Add user', ['class' =>  'btn btn-primary']) !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}

                </div>
            </div>
@endsection
