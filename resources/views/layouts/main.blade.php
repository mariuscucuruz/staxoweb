<!DOCTYPE HTML>
<html lang="en">

{{-- [BEGIN: header] --}}
@include('layouts.includes.header')
{{-- [END: header] --}}

{{-- [BEGIN: body] --}}
<body id="app-layout" class="skin-blue fixed" data-spy="scroll" data-target="#scrollspy">
<div class="wrapper">

    <header class="main-header">
    {{-- [BEGIN: topnav] --}}
    @include('layouts.includes.topnav')
    {{-- [END: topnav] --}}
    </header>

    <main>
    {{-- [BEGIN: mainbody] --}}
        <div class="container content-wrapper">
            <div class="row main-and-sidebar">

                <div class="container">
                    <div class="row">

                        {{-- [BEGIN: sidebar] --}}
                        @include('layouts.includes.sidebar')
                        {{-- [END: sidebar] --}}

                        {{-- [BEGIN: content] --}}
                        @include('layouts.includes.content')
                        {{-- [END: content] --}}

                    </div>
                </div>

            </div>
        </div>
    {{-- [END: mainbody] --}}
    </main>

{{-- [BEGIN: footer] --}}
@include('layouts.includes.footer')
{{-- [END: footer] --}}

</div>
</body>
{{-- [END: body] --}}
</html>
