    @if (!Auth::guest())
        <ol class="breadcrumb hidden-print">
            <li><a href="/dashboard">Dashboard</a></li>
        @hasSection('pagetitle')
            <li class="active">@yield('pagetitle')</li>
        @endif

            <li class="print" style="float: right;"><a href="javascript:void();" onclick="window.print();"><i class="fa fa-print"></i></a></li>
        </ol>
    @endif

            <article>
                <div class="content">
                    @yield('content')
                </div>
            </article>
        </div>
