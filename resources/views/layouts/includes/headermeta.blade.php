
    <meta charset="utf-8" />
    <meta http-equiv="content-type" content="text/html" />

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <link rel="shortcut icon" href="/favicon.ico" />

    <!--jQuery -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <!--jQuery UI -->
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.5/fullcalendar.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>

    <!-- bootstrap-table -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css" />
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>

    <!-- bootstrap datatables -->
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <!-- bootstrap //-->
    <link rel="stylesheet" type="text/css" media="all" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>{{-- need this twice here, otherwise it breaks... --}}

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/custom-print.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="//code.jquery.com/ui/1.11.4/themes/cupertino/jquery-ui.css" />
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

@stack('scripts-head')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Fonts -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
    <link href="//fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css' />
    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
    </style>

    <meta name="author" content="Marius Cucuruz" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />

@push('scripts')
    <script src="{{ asset('js/custom.js') }}"></script>
@endpush
@stack('scripts')

@push('styles')
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
@endpush
@stack('styles')
