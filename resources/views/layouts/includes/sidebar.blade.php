
            @if (!Auth::guest())
                <div class="col-md-3 col-md-offset-0 col-xs-3">
                    <section class="sidebar">
                            <ul>
                                @section('sidebar')
                                <li><a href="#">Sidebar item</a></li>
                                @show
                            </ul>
                    </section>
                </div>
            @endif
