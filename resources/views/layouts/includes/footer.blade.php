    <footer id="footer" class="hidden-print navbar-fixed-bottom">
            <div class="container hidden-print">
                <div class="row">
                    {{--<div class="col-md-3">
                        @section('footer')
                        @show
                    </div>--}}
                    <div class="col-md-12">
                        <div class="title" style="text-align: center;"><a href="{{ url('/') }}" target="_blank"><?=env('APP_TITLE') ?></a></div>
                        <p style="font-size: 10px; color: #6d7175; text-align: center; letter-spacing: .5px;">&trade; Marius Cucuruz for StaxoWeb </p>
                    </div>
                </div>
            </div>
    </footer>

    <!-- JavaScripts -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>  
    <![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6529676-24', 'auto');
  ga('send', 'pageview');
@if (Auth::user())
  ga('set', 'userId', '{{ Auth::user()->email }}'); // Set the user ID using signed-in user_id.
@endif
</script>
