    <nav class="navbar navbar-default navbar-static-top hidden-print">
        <div class="container-fluid hidden-print">
            <div class="navbar-header hidden-print">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                @if (Auth::user())
                <a class="navbar-brand" href="{{ url('/dashboard') }}" id="applogo">&nbsp;</a>
                @else
                <a class="navbar-brand" href="{{ url('/login') }}" id="applogo">&nbsp;</a>
                @endif
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
{{-- hide nav links for guest users: --}}
@if (Auth::user())
                <ul class="nav navbar-nav hidden-print" id="topnav_mainmenu">
                    <li class=""><a href="{{ url('/dashboard')}}" title="Go to Dahsboard"><i class="fa fa-btn fa-dashboard" aria-hidden="true"></i>Dashboard</a></li>

                    <li class=""><a href="{{ url('/users')}}" title="users overview"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> Users </a></li>
                    <li class=""><a href="{{ url('/addresses')}}" title="addresses overview"><i class="glyphicon glyphicon-home" aria-hidden="true"></i> Addresses </a></li>
                    <li class=""><a href="{{ url('/products')}}" title="products overview"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i> Products </a></li>
                    <li class=""><a href="{{ url('/orders')}}" title="orders overview"><i class="glyphicon glyphicon-barcode" aria-hidden="true"></i> Orders </a></li>
                </ul>

                    <div class="topnav_notifications nav navbar-nav navbar-right" id="topnav_usermenu" style="">
                        <div class="btn-group">
                            <a href="#" data-toggle="dropdown" data-original-title="Logout" data-placement="bottom" class="btn btn-metis-1 btn-sm">
                            <i class="fa fa-bars"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('/settings') }}"><i class="glyphicon glyphicon-user"></i> {{ Auth::user()->name }}</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
                            </ul>

                        </div>
                    </div>

@else
                <ul class="nav navbar-nav" id="topnav_mainmenu">
                    <li><a href="{{ url('/') }}">Home</a></li>
                </ul>
                <ul class=" nav navbar-nav navbar-right" id="topnav_usermenu">
                    <li><a href="{{ url('/login') }}" style="color: #FFF !important;"><i class="fa fa-btn fa-sign-in" style="color: #FFF !important;" aria-hidden="true"></i>Login</a></li>
                    <li><a href="{{ url('/register') }}" style="color: #FFF !important;"><i class="fa fa-btn fa-key" style="color: #FFF !important;" aria-hidden="true"></i>Register</a></li>
                </ul>
@endif
            </div>
        </div>
    </nav>

@if(Session::has('flash_message'))
    <div class="container">
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{ Session::get('flash_message') }}
        </div>
    </div>
@endif

@if(Session::has('flash_error'))
    <div class="container">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{ Session::get('flash_error') }}
        </div>
    </div>
@endif