<head>
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
@hasSection('pagetitle')
    <title>@yield('pagetitle')</title>
@else
    <title>StaxoWeb Test</title>
@endif

{{-- [BEGIN: headermeta] --}}
@include('layouts.includes.headermeta')
{{-- [END: headermeta] --}}

</head>
