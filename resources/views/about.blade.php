@extends('layouts.main')

@section('pagetitle', 'About Us Page')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">About Laravel 54</div>

                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in neque aliquam, vulputate quam sed, feugiat dui. Sed id sapien porttitor, scelerisque arcu quis, accumsan ligula. Donec congue molestie neque, a interdum odio hendrerit quis. Suspendisse potenti. Donec egestas tincidunt arcu. In sit amet dui pulvinar odio gravida tempor mollis nec augue. Fusce facilisis enim quis ornare tincidunt. Donec lacus eros, viverra aliquet pellentesque vel, suscipit quis augue. Nunc in mollis nulla. Aenean posuere sapien odio, et luctus eros bibendum vitae. Suspendisse non egestas massa. Ut non dolor ac velit iaculis dapibus vitae ac ex. Nulla iaculis aliquam orci in euismod.</p>
                    <p>In dolor est, tempor at consequat bibendum, ullamcorper vitae eros. Sed luctus auctor porta. Aliquam erat volutpat. Cras eget nisl diam. Nullam lobortis dui eu maximus viverra. Aenean elementum quis nisl quis pretium. Nullam aliquam egestas neque sit amet consectetur. Sed condimentum ut arcu vitae fermentum. Maecenas risus nibh, hendrerit ac ultricies nec, vehicula eu diam. Nunc sem eros, pretium quis nulla ac, aliquam ullamcorper tellus.</p>
                    <p>Aenean dapibus enim nec lectus vehicula placerat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam pretium ornare ante, sit amet convallis nisl sodales vel. Maecenas pretium congue sem, at interdum purus vulputate a. Vivamus feugiat a leo in accumsan. Vivamus dignissim tempor purus, in porttitor velit aliquam in. Etiam elementum, sapien quis ullamcorper dapibus, lorem est aliquam leo, at pellentesque nisl felis sit amet sapien. Integer placerat odio vel arcu finibus, quis varius ex euismod. Nulla facilisi. Cras ultricies nunc ex, quis venenatis tellus rhoncus et. In hac habitasse platea dictumst.</p>
                    <p>Nullam ornare justo quis quam fringilla laoreet at viverra ex. Sed aliquam tellus a convallis pellentesque. Curabitur mattis, nulla sit amet viverra placerat, mauris orci bibendum metus, ut convallis est nibh a sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In lobortis condimentum sem, id imperdiet metus consectetur eu. Nunc ornare sem nec ante congue tincidunt vel at mi. Praesent quis ultricies augue. Nullam sit amet lacus pretium, aliquet nisl eget, gravida lectus. Cras nec mattis ante. Sed id dapibus ipsum. Donec a lorem eros. Vivamus consectetur ac urna sit amet aliquet.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
