@extends('layouts.main')

@section('pagetitle')this is USER page {{ $user->name }}@endsection

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard {{$user->name}}</div>

                <div class="panel-body">

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if (count($messages) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($messages as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif


{!! Form::model($user, [
                'route'   => ['users.update', $user->id], 
                'method'  => 'put',
                'action'  => 'UserController@update',
                ]
            ) !!}
    <div class="form-group form-horizontal">
        <div class="form-group">
            {!! Form::label('name', 'Full Name', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('name', $user->name, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('phone', 'Phone', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('phone', $user->phone, ['class' => 'form-control']) !!}
            </div>  
        </div>
        <div class="form-group">
            {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::Text('mobile', $user->mobile, ['class' => 'form-control']) !!}
            </div>  
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
            </div>  
        </div>
        <hr />

    @if (Auth::user()->id == $user->id)
        <hr />
        <div class="form-group">
            {!! Form::label('password_old', 'Old Password', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password_old', null, ['class' => 'form-control']) !!}
            </div>  
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Password', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password', null, ['class' => 'form-control']) !!}
            </div>  
        </div>
        <div class="form-group">
            {!! Form::label('password_confirmation', 'Confirm password', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password_confirmation', null, ['class' => 'form-control']) !!}
            </div>  
        </div>
    @endif

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::submit('Update user', ['class' =>  'btn btn-primary']) !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}

                </div>
            </div>
@endsection

@section('content')
<p>user page</p>
@endsection


