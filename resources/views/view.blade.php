@extends('layouts.main')

@section('pagetitle')this is USER page {{ $user->name }}@endsection

@section('content')
            <div class="panel col-md-12">
                <div class="panel-heading">Dashboard {{$user->name}}</div>

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if (Session::get('message'))
                    <div class="alert alert-warning">
                        <h4>{{ Session::get('message') }}</h4>
                    </div>
                    @endif

                <div class="panel-body">
                    <dl class="dl-horizontal">
                            <dt>Full Name:</dt>
                            <dd>{{$user->name}}</dd>
                            <div class="clear"><br /></div>

                            <dt>Email:</dt>
                            <dd>{{$user->email}}</dd>
                            <div class="clear"><br /></div>

                            <dt>Phone:</dt>
                            <dd>{{$user->phone}}</dd>
                            <div class="clear"><br /></div>

                            <dt>Mobile:</dt>
                            <dd>{{$user->mobile}}</dd>
                            <div class="clear"><br /></div>
                    </dl>
                </div>


<div id="modal_editor" title="modal editor"></div>

                    </div>
                </div>
            @endif

                <div id="accordionCustOperations" class="accordion-group" >
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingCustOperations">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordionCustOperations" href="#collapseCustOperations" class="accordion-toggle" aria-expanded="false" aria-controls="collapseCustOperations">
                                <i class="fa fa-btn fa-caret-down" aria-hidden="true"></i> 
                                Operations for <strong>{{ $customer->name }}</strong> available to <strong>{{$user->name}}</strong>
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
@endsection
