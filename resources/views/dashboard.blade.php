@extends('layouts.main')

@section('title')Your Dashboard, {{ Auth::user()->name }}@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <p>You're logged in as <strong>{{ Auth::user()->name }}</strong> (level {{ Auth::user()->level }}).</p>
                    <p><br /></p>

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if (Session::get('message'))
                    <div class="alert alert-warning">
                        <h4>{{ Session::get('message') }}</h4>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
