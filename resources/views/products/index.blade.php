@extends('layouts.main')

@section('pagetitle') {{ @$pagetitle }}@endsection

@section('sidebar')
    @parent
    <li><a href="#">appended to sidebar test</a></li>
@stop

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">Users List</div>

                <div class="panel-body">
                        <div class="col-md-12 hidden-print">

                                @if (count($users))
<script type='text/javascript'>//<![CDATA[
function dateFormatter(value) {
    if (!value)
        return '';

    var formattedValue  = new Date(value * 1000);
        var YYYY        = formattedValue.getFullYear();
        var MM          = formattedValue.getMonth() + 1;
        var DD          = formattedValue.getDate();
        var hh          = formattedValue.getHours();
        var mm          = formattedValue.getMinutes();
        var ss          = formattedValue.getSeconds();

    return DD+'/'+MM+'/'+YYYY;
//return formatDate(new Date(formattedValue), '%H:%m:%s');
}

//]]> 
</script>
<style>
.fixed-table-container thead th .sortable {padding-right: 6px !important;}
</style>

                    <table id="myBSTable" class="table table-striped table-bordered table-hover table-condensed table-responsive"
data-toggle="table"
data-show-toggle="true"
data-show-columns="true"
data-show-footer="true"
data-show-refresh="false"
data-show-export="true"
data-detail-view="false"
data-sort-name="status"
data-sort-order="asc"
data-classes="table table-striped table-bordered table-hover table-condensed table-responsive"
data-striped="true"
data-pagination="true"
data-page-list="[15, 25, 50, 100, 200, ALL]"
data-page-size="15"
data-search="true">
                                <thead>
                                    <tr>
                                    	<th data-sortable="true" data-valign="middle" data-field="status">Status</th>
                                    	<th data-sortable="true" data-valign="middle" data-field="name">Name</th>
                                    	<th data-sortable="true" data-valign="middle" data-field="email">Email</th>
                                    	<th data-sortable="true" data-valign="middle" data-field="access_level">Access<br />Level</th>
                                    	<th data-sortable="true" data-valign="middle" data-field="created_at" data-formatter="dateFormatter">Created</th>
                                    	<th data-sortable="true" data-valign="middle" data-field="updated_at" data-formatter="dateFormatter">Last<br />Updated</th>
                                    </tr>
                                </thead>

                                <tbody class="">
                                    @forelse ($users as $user)
                                        <tr class="">
                                        	<td><em class="user-status-on}" ></em></td>
                                        	<td><a href="{{ url('users/'. $user->id .'/edit') }}" title="edit user {{$user->name}}">{{$user->name}}</a></td>
                                        	<td>{{ $user->email }}</td>
                                        	<td>{{ $user->updated_at->getTimestamp() }}</td>
                                        	<td>{{ $user->created_at->getTimestamp() }}</td>
                                        </tr>
                                    @empty
                                        <li>No users</li>
                                    @endforelse
                                </tbody>
                    </table>
                                        <div class="btn-group" role="group" style="text-align: center;">
                                                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                                                <a href="{{ url('users/create/')}}" class="btn btn-info">Add New User</a>
                                        </div>

                                @else
                                    <p>Blimey! There ain't no users</p>
                                @endif
                        </div>

                </div>
            </div>
@endsection

@section('footer')
    @parent
    <!--p>appended this to the footer</p//-->
@stop