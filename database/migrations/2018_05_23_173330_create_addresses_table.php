<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->index();
            /*$table->foreign('users_id')
                ->references('id')
                ->on('users');*/

            $table->string('postcode')->index();
            $table->string('street1')->nullable();
            $table->string('street2')->nullable();
            $table->string('county')->nullable();
            $table->string('country')->nullable();
            $table->integer('main')->nullable();
            $table->string('description')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
