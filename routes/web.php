<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

#Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', ['as' => 'homepage', function () {
    return redirect('dashboard');
    #return view('welcome');
}]);
Route::get('home/', function () {
    return redirect()->route('dashboard', ['p=home', 'nocache']);
});
Route::get('about/',                'PagesController@about');
Route::get('terms-and-conditions/', 'PagesController@terms');

// pages that require auth:
Route::group(['middleware' => ['web']], function () {
    Route::auth();

    Route::get('dashboard/',                    'HomeController@index');

    ## USERS:
    Route::resource('settings',                 'UserController@view');
    Route::resource('users',                    'UserController');

    ## CUSTOMERS:
    Route::get('customers/{id}',                ['as' => 'customers.view', 'uses' => 'CustomerController@show']);
    Route::put('customers/create',              'CustomerController@store');
    Route::resource('customers',                'CustomerController');

    ## Products:
    Route::put('products/create',               'ProductsController@store');
    Route::resource('products',                 'ProductsController');


    ## Orders:
    Route::put('orders/create',                 'OrdersController@store');
    Route::resource('orders',                   'OrdersController');


    ## Addresses:
    Route::put('addresses/create',               'AddressesController@store');
    Route::resource('addresses',                 'AddressesController');
/*
    ## MyOrders:
    Route::get('order/',                        'CustomerController@viewFleet');
    Route::get('order/view/{email}/{orderno}/', 'CustomerController@viewMachine')
                                                    ->where('email',   '[A-Za-z0-9]+')
                                                    ->where('orderno', '[0-9]+');
*/

});

// Log in/out routes:
#Route::get('login',  ['uses' => 'Auth\LoginController'])->name('login');
#Route::post('login', ['uses' => 'Auth\LoginController@login']);
#Route::get('logout', ['uses' => 'Auth\LoginController@logout']);

Route::get('login',   ['as' => 'login',  'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login',  ['as' => '',       'uses' => 'Auth\LoginController@login']);
Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);


// Password Reset Routes...
Route::post('password/email',           ['as' => 'password.email',  'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset',            ['as' => 'password.request','uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/reset',           ['as' => '',                'uses' => 'Auth\ResetPasswordController@reset']);
Route::get('password/reset/{token}',    ['as' => 'password.reset',  'uses' => 'Auth\ResetPasswordController@showResetForm']);

// Registration Routes...
Route::get('register',  ['as' => 'register',    'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('register', ['as' => '',            'uses' => 'Auth\RegisterController@register']);


// Registration Routes will be defined for each customer record (not globally allowed) - for added security 
#Route::get('register',  ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@showRegistrationForm'])->name('register');
#Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@register']);

// Registration confirmation
Route::get('registration-confirmed', 'PagesController@reg_confirmed');

// Password Reset Routes...
#Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
#Route::post('password/email',         ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
#Route::post('password/reset',         ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);

/***************************protected route************************/
Route::get('protected', ['middleware' => ['auth'], function() {
    return "this page requires that you be logged in";
}]);

/**
 * Available verbs:
 *  Route::get($uri, $callback);
 *  Route::post($uri, $callback);
 *  Route::put($uri, $callback);
 *  Route::patch($uri, $callback);
 *  Route::delete($uri, $callback);
 *  Route::options($uri, $callback);
 *  Route::match(['get', 'post'], '/', $callback); 
 *  Route::any('foo', $callback);
 *
 *  CLI Artisan command:
 *  php artisan make:controller NameController --resource
 */
